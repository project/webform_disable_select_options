(function($) {
  var setDisabledProp = function(element) {
    if ($.fn.prop) {
      $(element).prop('disabled', 'disabled');
    }
    $(element).attr('disabled', 'disabled');
  };
  var mutationObserverConfig = { attributes: true };
  var mutationObserverCallback = function(mutationsList) {
    for (var m in mutationsList) {
      var mutation = mutationsList[m];
      if (mutation.type === 'attributes' && mutation.attributeName === 'disabled') {
        var $target = $(mutation.target);
        if (mutation.target.disabled && $target.data('webform_disable_select_options_client_form')) {
          // Clear the debounce flag.
          $target.data('webform_disable_select_options_client_form', false);
        }
        else if (!mutation.target.disabled) {
          // Set the debounce flag so our modification of disabled doesn't cause
          // runaway recursion.
          $target.data('webform_disable_select_options_client_form', true);
          setDisabledProp($target);
        }
      }
    }
  };
  var mutationObserver = ('MutationObserver' in window) ?
    new MutationObserver(mutationObserverCallback) : false;
  Drupal.behaviors.webform_disable_select_options_client_form = {
    attach: function(context, settings) {
      $('[data-webform-disable-select-options]', context).once('webform_disable_select_options_client_form').each(function() {
        setDisabledProp(this);
        if (mutationObserver) {
          mutationObserver.observe(this, mutationObserverConfig);
        }
        else {
          $(this.form).once('webform_disable_select_options_client_form').bind('change', function() {
            $('[data-webform-disable-select-options]', this).each(function() {
              var _this = this;
              setTimeout(function() { setDisabledProp(_this); }, 0);
            });
          });
        }
      });
    }
  };
})(jQuery);
