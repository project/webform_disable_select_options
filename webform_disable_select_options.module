<?php

/**
 * @file
 * Core functionality of the Webform Disable Select Options module.
 */

/**
 * Implements hook_form_FORM_ID_alter() for webform-components-form.
 *
 * Provide the UI for disabling/enabling Select Options options.
 */
function webform_disable_select_options_form_webform_component_edit_form_alter(&$form, &$form_state, $form_id) {
  if (!empty($form_state['build_info']['args']) && count($form_state['build_info']['args']) == 3) {
    list($node, $component,) = $form_state['build_info']['args'];
    $disable_select_options = '';
    if (isset($component['extra']['disable_select_options'])) {
      $disable_select_options = $component['extra']['disable_select_options'];
    }
    foreach (array('input', 'values') as $key) {
      if (isset($form_state[$key]['extra']['disable_select_options'])) {
        $disable_select_options = $form_state[$key]['extra']['disable_select_options'];
        break;
      }
    }
    $form['extra']['disable_select_options'] = array(
      '#title' => t('Disabled Option (Keys)'),
      '#description' => t('Enter the <em>keys</em> (not displayed labels), one per line, of options to show but disable. This allows the option to remain visible, retains submitted values in the database (unlike deleting the option), but prevents new submissions from using the specified options.'),
      '#type' => 'textarea',
      '#value' => $disable_select_options,
    );
  }
}

/**
 * Implements hook_webform_component_defaults_alter().
 */
function webform_disable_select_options_webform_component_defaults_alter(&$defaults, $type) {
  if ($type === 'select') {
    $defaults['extra']['disable_select_options'] = '';
  }
}

/**
 * Implements hook_webform_component_render_alter().
 *
 * Attach our post_render callback to modify the built HTML.
 */
function webform_disable_select_options_webform_component_render_alter(&$element, &$component) {
  if ($component['type'] === 'select') {
    if (isset($component['extra']['disable_select_options'])) {
      $disable_select_options = _webform_disable_select_get_values($component['extra']['disable_select_options']);
      if ($disable_select_options !== FALSE) {
        if (!isset($element['#post_render'])) {
          $element['#post_render'] = array();
        }
        $element['#post_render'][] = '_webform_disable_select_options_element_postrender';
        if (!isset($element['#element_validate'])) {
          $element['#element_validate'] = array();
        }
        $element['#element_validate'][] = '_webform_disable_select_options_element_validate';
        // Pass along our parsed values for the postrender callback.
        $element['#disable_select_options'] = $disable_select_options;
        // Attach our JS to handle responding to any Webform conditional logic
        // (if Webform has to hide then show this element at any time, it will
        // remove any "disabled" attribute).
        if (!isset($element['#attached'])) {
          $element['#attached'] = array();
        }
        if (!isset($element['#attached']['js'])) {
          $element['#attached']['js'] = array();
        }
        $element['#attached']['js'][] = drupal_get_path('module', 'webform_disable_select_options') . '/js/webform_client_form.js';
      }
    }
  }
}

/**
 * Element post-render callback for Select Options component.
 *
 * Disable the options specified in the component's configuration screen.
 *
 * @param string $markup
 *   The rendered markup for the component.
 * @param array $element
 *   The render element representing the component.
 *
 * @return string
 *   The rendered markup, after any modification.
 */
function _webform_disable_select_options_element_postrender($markup, array $element) {
  if (isset($element['#disable_select_options'])) {
    if ($element['#disable_select_options'] !== FALSE) {
      foreach ($element['#disable_select_options'] as $key) {
        /* Select Options may appear in many formats, where XYZ is the $key:
         * - <option value="XYZ">...</option>
         * - <input value="XYZ" type="radio"...>
         * - <input value="XYZ" type="checkbox"...>
         */
        // All values should have been checked by
        // _webform_disable_select_get_values() so we do not need to check.
        $key = preg_quote($key, '/');
        $pattern = "/(<(input|option)[^>]+value=['\"]{$key}['\"])/";
        $markup = preg_replace($pattern, '\1 disabled="disabled" data-webform-disable-select-options', $markup);
      }
    }
  }
  return $markup;
}

/**
 * Element validate callback for Select Options component.
 *
 * Enforce disabled options specified in the component's configuration screen.
 *
 * @param array $element
 *   Form element being validated.
 * @param array $form_state
 *   Form state.
 * @param array $form
 *   Form definition.
 */
function _webform_disable_select_options_element_validate(array $element, array &$form_state, array $form) {
  if (isset($element['#webform_component']['extra']['disable_select_options'])) {
    $disable_select_options = _webform_disable_select_get_values($element['#webform_component']['extra']['disable_select_options']);
    if ($disable_select_options !== FALSE) {
      $bad_values = array_intersect((array) $element['#value'], $disable_select_options);
      if (!empty($bad_values)) {
        // (One of) The selected value has been disabled.
        form_error($element, t('The chosen value is unavailable. Please choose a different value.'));
      }
    }
  }
}

/**
 * Utility function to parse out an array of values from a chunk of text.
 *
 * @param string $text
 *   String with newline-separated values.
 *
 * @return array|bool
 *   Parsed values (lines) from input, or FALSE if no values present.
 */
function _webform_disable_select_get_values($text) {
  $text = trim($text);
  if ($text === '') {
    return FALSE;
  }
  $values = preg_split('/[ \t\v]*[\\r\\n]+[ \t\v]*/', $text, -1, PREG_SPLIT_NO_EMPTY);
  if (empty($values)) {
    return FALSE;
  }
  $return = array();
  // There is at least one item in an array of strings.
  foreach ($values as $value) {
    // A key may be "0" or some other FALSE-y thing. We cannot use empty().
    $value = trim($value);
    if ($value !== '') {
      $return[] = $value;
    }
  }
  if (empty($return)) {
    return FALSE;
  }
  return $return;
}
